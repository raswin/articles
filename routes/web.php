<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'ArticlesController@getArticles')->name('articles');
Route::get('/create_article', 'ArticlesController@createArticle')->name('create_article');
Route::post('/save_article', 'ArticlesController@saveArticle')->name('save_article');
Route::get('/article/{slug}', 'ArticlesController@getArticle')->name('get_article');
