@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @foreach($articles as $article)
        <div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
            <div class="article">
                <div class="card-body">
                    <h4 class="card-title">
                        <a href="{{ route('get_article', $article->slug) }}">{{ $article->title }}</a>
                    </h4>
                    <p class="card-text">{{ str_limit($article->description, 60, '...') }}</p>
                    <p>Author: {{ $article->author->name }}</p>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <div class="row">
        {!! $articles->links() !!}
    </div>
</div>
@endsection
