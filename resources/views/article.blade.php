@extends('layouts.app')

@section('content')
    <div class="container">
        @if(session()->has('message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="row">
            <div class="col-lg-8">
                <h1 class="mt-4">{{ $article->title }}</h1>
                <p class="lead">by {{ $article->author->name }}</p>
                <hr>
                <p>Posted on {{ $article->created_at->formatLocalized('%A %d %B %Y') }}</p>
                <hr>
                <p>{{ $article->description }}</p>
            </div>
        </div>
    </div>
@endsection
