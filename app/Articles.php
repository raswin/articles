<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Articles extends Model
{
    protected $table = 'articles';
    protected $fillable = [
        'title',
        'slug',
        'description',
        'user_id'
    ];

    public function author(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
