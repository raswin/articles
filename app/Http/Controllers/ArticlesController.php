<?php

namespace App\Http\Controllers;

use App\Articles;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getArticles()
    {
        $articles = Articles::with('author')->paginate(8);

        return view('articles')->with('articles', $articles);
    }

    public function getArticle($slug)
    {
        $article = Articles::where('slug', $slug)->with('author')->first();

        return view('article')->with('article', $article);
    }

    public function createArticle()
    {
        return view('create_article');
    }

    public function saveArticle(Request $request)
    {
        $this->validate($request, array(
            'title'         => 'required|min:6|max:64',
            'description'   => 'required|min:32',
            'slug'          => 'unique:articles'
        ));

        $article = Articles::create([
            'title' => $request->input('title'),
            'slug' => ($request->input('slug')) ? str_slug($request->input('slug'), '_') : str_slug($request->input('title'), '_').'_'.time(),
            'description' => $request->input('description'),
            'user_id' =>  Auth::user()->id,
        ]);

        return redirect()->route('get_article', $article->slug)->with('message', 'Success');
    }
}
